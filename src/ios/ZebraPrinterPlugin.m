/********* ZebraPrinterPlugin.m Cordova Plugin Implementation *******/

#import "ZebraPrinterPlugin.h"
#import "TcpPrinterConnection.h"
#import "ZebraPrinterFactory.h"
#import "ZebraPrinter.h"
#import "ZebraPrinterConnection.h"
#import "PrinterStatus.h"
#import <Cordova/CDV.h>
#import <UIKit/UIKit.h>



@implementation ZebraPrinterPlugin

- (void)echo:(CDVInvokedUrlCommand*)command {
    CDVPluginResult *pluginResult = nil;

    NSArray *arguments = [command arguments];
    NSString *value = arguments[0];

    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:value];

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)printZpl:(CDVInvokedUrlCommand*)command {

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        CDVPluginResult *pluginResult = nil;
        NSArray*  arguments           = [command arguments];
        NSString *ipaddress = [arguments objectAtIndex:0];
        NSArray *bclabels=[arguments objectAtIndex:1];

        if (ipaddress != nil && [ipaddress length] > 0) {
        
            // Instantiate connection for ZPL TCP port at given address. 
            id<ZebraPrinterConnection, NSObject> thePrinterConn = [[TcpPrinterConnection alloc] initWithAddress:ipaddress andWithPort:9100];
            
            // Open the connection - physical connection is established here.
            BOOL success = [thePrinterConn open];
            
            // This example prints "This is a ZPL test." near the top of the label.
        //NSString *zplData = @"^XA^FO20,20^A0N,25,25^FDThis is a ZPL test.^FS^XZ";
        
            //NSString *zplData;
            NSString *successMsg = @"Print Success";
            NSError *error = nil;
            
            // Send the data to printer as a byte array.
            
            
            for (NSString *zplData in bclabels){
                success = success && [thePrinterConn write:[zplData dataUsingEncoding:NSUTF8StringEncoding] error:&error];
            }
            
            /*
            
            for (NSInteger charIdx=0; charIdx<bclabels.length; charIdx++) {
                    // Do something with character at index charIdx, for example:
                        //zplData=[bclabels characterAtIndex:charIdx];
                        NSLog(@"%C",[bclabels characterAtIndex:charIdx] );
                    success = success && [thePrinterConn write:[[bclabels characterAtIndex:charIdx] dataUsingEncoding:NSUTF8StringEncoding] error:&error];
                    
                    
                } */
        
        
            
            if (success == YES || error == nil) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:successMsg];
            }else if (success != YES || error != nil) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
            }
            // Close the connection to release resources.
            [thePrinterConn close];
            //[thePrinterConn release];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }

        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    });
}

- (void)connectPrinterTCPIP:(CDVInvokedUrlCommand *)command {

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        CDVPluginResult *pluginResult = nil;
        
        NSArray*  arguments = [command arguments];
        NSString *ipaddress = [arguments objectAtIndex:0];
        
        NSString *status = @"";
        if (ipaddress != nil && [ipaddress length] > 0) {
            TcpPrinterConnection *zebraPrinterConnection = [[TcpPrinterConnection alloc] initWithAddress:ipaddress andWithPort:9100];
            BOOL success = [zebraPrinterConnection open];
            
            if(success){
                status = @"connected";
            }else {
                status = @"not found";
            }
            
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:status];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    });
}

- (void)checkPrinterStatus:(CDVInvokedUrlCommand *)command {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        CDVPluginResult *pluginResult = nil;

        NSArray*  arguments = [command arguments];
        NSString *ipaddress = [arguments objectAtIndex:0];
        
        if (ipaddress != nil && [ipaddress length] > 0) {
        
            TcpPrinterConnection *zebraPrinterConnection = [[TcpPrinterConnection alloc] initWithAddress:ipaddress andWithPort:9100];
    //        BOOL success = [zebraPrinterConnection open];
            NSError *error = nil;
            id<ZebraPrinter, NSObject> printer = [ZebraPrinterFactory getInstance:zebraPrinterConnection error:&error];
            
            NSString *status = @"";
            PrinterStatus *printerStatus = [printer getCurrentStatus:&error];
            if (printerStatus.isReadyToPrint) {
                status = @"Ready To Print";
            } else if (printerStatus.isPaused) {
                status = @"Cannot Print because the printer is paused.";
            } else if (printerStatus.isHeadOpen) {
                status = @"Cannot Print because the printer head is open.";
            } else if (printerStatus.isPaperOut) {
                status = @"Cannot Print because the paper is out.";
            } else {
                status = @"Cannot find printer.";
            }
            // NSLog(printerStatus);
            NSLog(ipaddress);
            NSLog(status);
            
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:status];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }

        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    });
}

@end
