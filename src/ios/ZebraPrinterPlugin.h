//
//  ZebraPrinterPlugin.h

//
//  Created by Kenneth on 12/6/2018.
//
//

#import <Cordova/CDVPlugin.h>
#import <Cordova/CDV.h>
#import <UIKit/UIKit.h>
@interface ZebraPrinterPlugin : CDVPlugin

- (void)echo:(CDVInvokedUrlCommand*)command;
-(void)printZpl:(CDVInvokedUrlCommand*)command;
- (void)checkPrinterStatus:(CDVInvokedUrlCommand *)command;

@end
